# Introduction to JavaScript and Python

Practical introduction into programming for beginners. Cross-learning two different programming languages at once allows for a deeper understanding of programming principles.

- [Russian](ru/learn-javascript-and-python.md)
